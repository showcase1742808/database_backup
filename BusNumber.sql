-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 03, 2024 at 12:42 PM
-- Server version: 10.6.17-MariaDB-cll-lve
-- PHP Version: 8.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pioneerclassflex_ict`
--

-- --------------------------------------------------------

--
-- Table structure for table `BusNumber`
--

CREATE TABLE `BusNumber` (
  `BusNumberID` int(11) NOT NULL,
  `BusNumber` varchar(20) NOT NULL,
  `Capacity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `BusNumber`
--

INSERT INTO `BusNumber` (`BusNumberID`, `BusNumber`, `Capacity`) VALUES
(1, 'BUS101', 50),
(2, 'BUS101', 50),
(3, 'BUS102', 40);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BusNumber`
--
ALTER TABLE `BusNumber`
  ADD PRIMARY KEY (`BusNumberID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `BusNumber`
--
ALTER TABLE `BusNumber`
  MODIFY `BusNumberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
